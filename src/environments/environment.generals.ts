export const environment = {
    production: false,

    regex_mail: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,

    menu_items: [{ label: 'Personajes' }, { label: 'Planetas' }]
};