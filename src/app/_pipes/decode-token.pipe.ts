import { Pipe, PipeTransform } from '@angular/core';
import { UserInterface } from 'src/app/_interfaces';

@Pipe({
  name: 'decodeToken'
})
export class DecodeTokenPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    // let _base64 = value.split('.')[1].replace('-', '+').replace('_', '/');
    // let _info: UserInterface = JSON.parse(window.atob(_base64));
    // return _info.surename + ' ' + _info.name;
    let _name = value.split(' ');
    return _name[1] + ' ' + _name[0];
  }

}
