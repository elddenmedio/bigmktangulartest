import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_components/public/login/login.component';

import { AuthGuard } from 'src/app/_guards';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'articles', loadChildren: () => import('./_components/articles/articles.module').then(m => m.ArticlesModule), canActivate: [AuthGuard] },
  { path: 'articles/:elementID', loadChildren: () => import('./_components/articles/articles.module').then(m => m.ArticlesModule), canActivate: [AuthGuard] },
  
  { path: '', redirectTo: 'articles', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
