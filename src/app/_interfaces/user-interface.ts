export interface UserInterface {
    id?: number;
    name: string;
    surname: string;
    role: string;
    
    [key: string]: any;
}
