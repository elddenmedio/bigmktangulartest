export interface ArticlesInterface {
    id: string | number;
    title: string;
    author: string;
    content?: string;
}
