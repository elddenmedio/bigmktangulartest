export interface GenericInterface {
    status: number; 
    message?: string;
    msgError?: string
    data?: any
}
