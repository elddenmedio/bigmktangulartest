import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginService, SessionService } from 'src/app/_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  usr;
  //-------------
  username;
  password;
  //-------------
  errLogin: String = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private loginServices: LoginService,
    private authService: SessionService
  ) {
    this.usr = {};
  }

  ngOnInit() {
    this.createControls();
    this.createForm();
  }

  private createControls() {
    this.username = new FormControl('', [
      // Validators.email,
      // Validators.pattern(GENERALS.regex_mail),
      Validators.required
    ]);

    this.password = new FormControl('', [
      Validators.required
    ]);
  }

  private createForm() {
    this.form = this.formBuilder.group({
      username: this.username,
      password: this.password
    });
  }

  get f() { return this.form.controls; }

  login(): void {
    this.loginServices.login(this.form.value).subscribe(
      succ => {
        this.loginServices.getUsereInfo().subscribe(
          userInfo => {
            this.authService.setSession(userInfo);
            this.router.navigate(['/articles']);
          },
          err => {
            this.errLogin = err.error.message;
            this.clearErr();
          }
        );
      },
      err => {
        this.errLogin = err.error.message;
        this.clearErr();
      }
    );
  }

  private clearErr(): void {
    setTimeout(() => {
      delete this.errLogin;
    }, 7000);
  }
}
