import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/_services';
import { LoginService } from 'src/app/_services';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  session: string;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
    this.getSession();
  }

  private getSession(): void {
    this.session = this.sessionService.getToken();
  }

  logout(): void {
    this.loginService.logOut().subscribe(
      succ => {
        this.sessionService.deleteSession();
        this.router.navigate(['/']);
      }
    );
  }
}
