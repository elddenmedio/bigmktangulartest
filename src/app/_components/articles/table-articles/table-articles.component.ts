import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ArticlesInterface, GenericArticlesInterface } from 'src/app/_interfaces';
import { ArticlesService, JumpToService } from 'src/app/_services';

@Component({
  selector: 'table-articles',
  templateUrl: './table-articles.component.html',
  styleUrls: ['./table-articles.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class TableArticlesComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['title', 'author'];
  dataSource = new MatTableDataSource<GenericArticlesInterface>();
  lengthPagination: number = 0;
  page: number = 1;
  count: number = 10;
  elementID: number = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private articleServices: ArticlesService,
    private jumpToService: JumpToService
  ) {
    if (this.route.snapshot.params.elementID) {
      this.elementID = this.route.snapshot.params.elementID;
      // this.jumpToService.jump('elementContent');
    }
    this.router.events.subscribe((event: any) => { 
      if (event instanceof NavigationStart) {
        let _url = event.url.split('/');
        this.elementID = Number(_url[_url.length - 1]);
      }
    });

  }

  ngOnInit(): void {
    this.loadInfo(true);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  private loadInfo(refresh: boolean): void {
    this.dataSource = new MatTableDataSource<GenericArticlesInterface>();
    this.lengthPagination = 0;
    if (refresh) {
      this.page = 1;
      this.count = 10;
    }

    this.articleServices.getInfo(this.page, this.count).subscribe(
      succ => {
        this.dataSource = new MatTableDataSource(succ.articles);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.lengthPagination = succ.articles.length;
      }
    );
  }

  changePage(event: MatPaginator): void {
    this.page = (event.pageIndex + 1);
    this.count = event.pageSize;
    event.pageSize

    this.loadInfo(false);
  }

  displayContent(elementID: number): void {
    this.elementID = null;
    this.router.navigate(['/articles/' + elementID]);
  }
}
