import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ArticlesService } from 'src/app/_services';
import { ArticlesInterface } from 'src/app/_interfaces';

@Component({
  selector: 'table-content',
  templateUrl: './table-content.component.html',
  styleUrls: ['./table-content.component.scss']
})
export class TableContentComponent implements OnInit {
  @Input() id: string;
  info: ArticlesInterface;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private articleService: ArticlesService
  ) {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        let _url = event.url.split('/');
        this.id = _url[_url.length - 1];

        this.getInfo();
      }
    });
  }

  ngOnInit(): void {
    this.getInfo();
  }

  private getInfo(): void {
    this.articleService.getArticleInfo(this.id).subscribe(
      articles => {
        // this.info = succ.articles;
        articles.articles.forEach(
          el => {
            if (el['id'] == this.id) {
              this.info = el;
            }
          }
        );
      },
      err => {
        console.error(err);
      }
    );
  }
}
