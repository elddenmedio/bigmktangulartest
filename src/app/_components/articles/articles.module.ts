import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';

import { MenuComponent } from './../../_components/private/menu/menu.component';
import { ArticlesComponent } from './articles.component';
import { TableArticlesComponent } from './table-articles/table-articles.component';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';
import { TableContentComponent } from './table-content/table-content.component';

@NgModule({
  declarations: [
    MenuComponent,
    ArticlesComponent, 
    TableArticlesComponent, TableContentComponent
  ],
  imports: [
    CommonModule,
    ArticlesRoutingModule,

    CommonLocalModule
  ]
})
export class ArticlesModule { }
