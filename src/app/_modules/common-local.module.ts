import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { PrimengModule } from './primeng.module';

import { AuthInterceptorService, JumpToService, SessionService } from 'src/app/_services';

import { DecodeTokenPipe } from '../_pipes';

const modules = [
  HttpClientModule,
  FormsModule,
  ReactiveFormsModule,
  MaterialModule,
  PrimengModule
]

@NgModule({
  declarations: [
    DecodeTokenPipe
  ],
  imports: [
    CommonModule,

    modules
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }, 
    
    SessionService,
    JumpToService
  ],
  exports: [
    CommonModule,

    modules,

    DecodeTokenPipe
  ]
})
export class CommonLocalModule { }
