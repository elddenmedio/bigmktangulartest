import { TestBed } from '@angular/core/testing';

import { JumpToService } from './jump-to.service';

describe('JumpToService', () => {
  let service: JumpToService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JumpToService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
