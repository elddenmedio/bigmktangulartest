import { Injectable } from '@angular/core';

const TOKEN = 'user-token';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  setSession(user: any): void {
    this.setToken(user.firstName + ' ' + user.lastName);
  }

  setToken(token: string): void {
    sessionStorage.setItem(TOKEN, token);
  }

  getToken(): any {
    return sessionStorage.getItem(TOKEN);
  }

  deleteSession(): void {
    sessionStorage.removeItem(TOKEN);
  }
}
