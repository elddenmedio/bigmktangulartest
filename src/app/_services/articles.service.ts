import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { GenericArticlesInterface } from 'src/app/_interfaces';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(
    private http: HttpClient
  ) { }

  getInfo(page: number, offset: number): Observable<GenericArticlesInterface> {
    return this.http.get<GenericArticlesInterface>(environment.end_point_api + 'articles', { params: { page: page.toString(), offset: offset.toString() } });
  }

  getArticleInfo(articleID: string): Observable<GenericArticlesInterface> {
    return this.http.get<GenericArticlesInterface>(environment.end_point_api + 'articles');
  }
}
