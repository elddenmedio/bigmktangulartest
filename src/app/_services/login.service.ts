import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { GenericInterface } from 'src/app/_interfaces';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  login(form: any): Observable<GenericInterface> {
    let _body = { username: form.username, password: form.password };
    return this.http.post<GenericInterface>(environment.end_point_api + 'auth', {}, { params: _body });
  }

  getUsereInfo(): Observable<GenericInterface> {
    return this.http.get<GenericInterface>(environment.end_point_api + 'auth');
  }

  logOut(): Observable<GenericInterface> {
    return this.http.get<GenericInterface>(environment.end_point_api + 'logout');
  }
}
